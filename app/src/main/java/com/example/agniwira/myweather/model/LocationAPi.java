package com.example.agniwira.myweather.model;

/**
 * Created by agni.wira on 10/07/17.
 */

public class LocationAPi {
    private String name;

    public LocationAPi(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
