package com.example.agniwira.myweather.service;

import com.example.agniwira.myweather.model.Weather;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by agni.wira on 10/07/17.
 */

public interface WeatherService {
    @GET("v1/current.json")
    Call<Weather> listWeather(@Query("key") String api, @Query("q") String city);
}
