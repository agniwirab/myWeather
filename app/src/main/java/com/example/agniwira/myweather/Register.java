package com.example.agniwira.myweather;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.agniwira.myweather.helper.DatabaseHelper;
import com.example.agniwira.myweather.model.UserModel;

public class Register extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final DatabaseHelper dbHelper = new DatabaseHelper(Register.this);

        Button submitBtn = (Button) findViewById(R.id.buttonSubmit);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = ((EditText) findViewById(R.id.edittext1)).getText().toString();
                String password = ((EditText) findViewById(R.id.edittext2)).getText().toString();

                if(dbHelper.isUserExist(username)){
                    Toast.makeText(getApplicationContext(), "username sudah terpakai", Toast.LENGTH_SHORT).show();
                }else{
                    dbHelper.insertUser(new UserModel(username,password));
                    Intent intent = new Intent(Register.this,MainActivity.class);
                    (Register.this).startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Pendaftaran berhasil", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }
}
